# -----------
# Wed,  5 Feb
# -----------

"""
Cold Calling
	you're only called ONCE per rotation
	it's totally fine to be wrong, the idea is to discuss and to learn

FoCS
	please track and go
	really worth your time

Lab Sessions
	Tue, 6-8pm, GDC 1.302
	Thu, 6-8pm, GDC 1.302

Office Hours
	Glenn
		MF, 12-12:45pm, GDC 6.302
		T,	 3-4pm,		Zoom, https://us04web.zoom.us/j/412178924

	Chenxi
		F, 2:30-3:30pm, GDC 1.302

	Hannah
		M, 2-3pm, GDC 1.302

	Abhinav
		W, 2-3pm, GDC 1.302

	Alvin
		T, 1-2pm, GDC 1.302

Canvas
	personal questions

Piazza
	class questions
	ask and answer questions
	please be proactive

Notes
	these notes are on GitLab and Piazza
"""

i = 2
print(type(i))    # int
print(type(int))  # type
print(type(type)) # type

"""
list, like Java ArrayList
front-loaded array
order matters
cost of insertion at the front:  linear
cost of insertion at the back:   amortized const
cost of insertion at the middle: linear
cost of removal at the front:    linear
cost of removal at the back:     const
cost of removal at the middle:   linear
cost indexing:                   const
"""

"""
Java   has homogenous    containers
Python has heterogeneous containers
"""

"""
tuple, an immutable list
can't change the content or size
"""

"""
set, like Java HashSet
hashtable
no duplicates
order doesn't matter
elements have to be hashable
the hashables are the immutables (int, float, complex, str, tuple, frozenset)
can't change the content
cost of insertion: amortized const
cost of removal:   const
cost of indexing:  can't
"""

"""
frozenset, an immutable set
can't change the content or size
"""

"""
dict, like Java HashMap
hashtable
a set of keys with corresponding values
can't change the keys
"""

"""
deque, like Java LinkedList
linked list
order matters
cost of insertion: const
cost of removal:   const
cost of indexing:  linear
"""
