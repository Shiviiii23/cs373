# -----------
# Fri, 14 Feb
# -----------

"""
Cold Calling
	you're only called ONCE per rotation
	it's totally fine to be wrong, the idea is to discuss and to learn

FoCS
	please track and go
	really worth your time

Lab Sessions
	Tue, 6-8pm, GDC 1.302
	Thu, 6-8pm, GDC 1.302

Office Hours
	Glenn
		MF, 12-12:45pm, GDC 6.302
		T,	 3-4pm,		Zoom, https://us04web.zoom.us/j/412178924

	Chenxi
		F, 2:30-3:30pm, GDC 1.302

	Hannah
		M, 2-3pm, GDC 1.302

	Abhinav
		W, 2-3pm, GDC 1.302

	Alvin
		T, 1-2pm, GDC 1.302

Canvas
	personal questions

Piazza
	class questions
	ask and answer questions
	please be proactive

Notes
	these notes are on GitLab and Piazza
"""

for v in a : # an iterable
    f(v)

# Java

LinkedList<Integer> x = new LinkedList<Integer>(...)
...
Iterator<Integer> p = x.iterator();
while (p.hasNext()) {
    Integer v = p.next();
    f(v);}

# Python

for v in a : # an iterable
    f(v)

a = <an iterable>
p = iter(a)            # same as a.__iter__()
try :
    while True :
        v = next(p)    # same as p.__next__()
except StopIteration :
    pass

"""
reduce takes three arguments:
    bf: a binary function
    a:  an iterable
    v:  a value (seed)

v <bf> a0 <bf> a1 <bf> a2 <bf> ... <bf> an-1
"""

"""
bf: +
a:  a set of integers
v:  0
the sum
"""

"""
bf: *
a:  a set of integers
v:  1
the product
"""

"""
bf: *
a:  a set of integers, 1..n
v:  1
the factorial
"""

"""
indexable implies iterable, but not the other way around
for example, a set is iterable, but not indexable
"""
