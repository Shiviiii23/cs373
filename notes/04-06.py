# -----------
# Mon,  6 Apr
# -----------

"""
Cold Calling
	you're only called ONCE per rotation
	it's totally fine to be wrong, the idea is to discuss and to learn

FoCS
	please track and go
	really worth your time

Canvas
	personal questions

Piazza
	class questions
	ask and answer questions
	please be proactive

Notes
	these notes are on GitLab and Piazza
"""

"""
cross join
takes two arguments
    a table
    another table
produces
    a table with ALL of the rows of table 1 connected to all of the rows of table 2
"""
