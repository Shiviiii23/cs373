# -----------
# Mon, 17 Feb
# -----------

"""
Cold Calling
	you're only called ONCE per rotation
	it's totally fine to be wrong, the idea is to discuss and to learn

FoCS
	please track and go
	really worth your time

Lab Sessions
	Tue, 6-8pm, GDC 1.302
	Thu, 6-8pm, GDC 1.302

Office Hours
	Glenn
		MF, 12-12:45pm, GDC 6.302
		T,	 3-4pm,		Zoom, https://us04web.zoom.us/j/412178924

	Chenxi
		F, 2:30-3:30pm, GDC 1.302

	Hannah
		M, 2-3pm, GDC 1.302

	Abhinav
		W, 2-3pm, GDC 1.302

	Alvin
		T, 1-2pm, GDC 1.302

Canvas
	personal questions

Piazza
	class questions
	ask and answer questions
	please be proactive

Notes
	these notes are on GitLab and Piazza
"""

a = [2, 3, 4]

for v in a : # a must be iterable
    ...

a = [[2, 3], [4, 5], [6, 7]]

for u, v in a : # a must be iterable over iterables of len 2
    ...

"""
call f on the first element of a
call g on the rest of the elements of a
"""

# this works

a = deque([2, 3, 4])
print(type(a))       # deque

p = iter(a)
print(type(p)) # deque iterator
print(a is p)  # false

f(next(p))

try :
    while True :
        g(next(p))
except StopIteration :
    pass

# this does NOT work

a = deque([2, 3, 4])
p = iter(a)

f(next(p))

for v in a : # no, calls g on first element
    g(v)

# this works again

a = deque([2, 3, 4])
p = iter(a)

q = iter(p)
print(type(q)) # deque iterator
print(p is q)  # true

f(next(p))

for v in p :
    g(v)
