# -----------
# Mon, 30 Mar
# -----------

"""
Cold Calling
	you're only called ONCE per rotation
	it's totally fine to be wrong, the idea is to discuss and to learn

FoCS
	please track and go
	really worth your time

Lab Sessions
	Tue, 6-8pm, GDC 1.302
	Thu, 6-8pm, GDC 1.302

Office Hours
	Glenn
		MF, 12-12:45pm, GDC 6.302
		T,	 3-4pm,		Zoom, https://us04web.zoom.us/j/412178924

	Chenxi
		F, 2:30-3:30pm, GDC 1.302

	Hannah
		M, 2-3pm, GDC 1.302

	Abhinav
		W, 2-3pm, GDC 1.302

	Alvin
		T, 1-2pm, GDC 1.302

Canvas
	personal questions

Piazza
	class questions
	ask and answer questions
	please be proactive

Notes
	these notes are on GitLab and Piazza
"""

"""
algebras
    a set of elements
    a set of operations

integer arithmetic
    integers
    +, -, *, /, %
    closed on +, -, *, %
    open   on /

relational algebra
    a table (relation)
    select, project, join (many kinds of join)
    closed on ALL operations
"""

"""
a movie table
title year director genre
"star wars" 1977 "george lucas"   "sci-fi"
"shane"     1953 "george stevens" "western"
...
"""

"""
select
takes two arguments
    a table
    a unary predicate (a one-arg function that returns a bool)
produces
    a table with ALL of the cols, but only SOME of the rows

select(the movie table, where director is "george lucas")
"""

"""
a poor-man's representation of a table in Python
"""

# not quite right
movies =
{
{"title": "star wars", "year": 1977, "director", "george lucas", "genre": "sci-fi"}
...
}

"""
can't have a set of dicts, because dicts are not hashable
we'll have to use a list
"""

movies =
[
{"title": "star wars", "year": 1977, "director", "george lucas", "genre": "sci-fi"}
...
]

def select (r, f) :
    for d in r :
        if f(d) :
            yield d

del select (r, f) :
    return (d for d in r if f(d))

def select (r, f) :
    return filter(f, r)

movies2 = select(movies, lambda movie_dict : movie_dict["director"] == "george lucas")

"""
project
takes many arguments
    a table
    a series of attributes
produces
    a table with ALL of the rows, but only SOME of the cols
"""

movies =
[
{"title": "star wars", "year": 1977, "director", "george lucas", "genre": "sci-fi"}
...
]

movies2 = project(movies, "genre", "year")

# something like

movies2 =
[
{"genre": "sci-fi", "year": 1977}
...
]
