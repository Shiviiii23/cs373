# -----------
# Fri,  7 Feb
# -----------

"""
Cold Calling
	you're only called ONCE per rotation
	it's totally fine to be wrong, the idea is to discuss and to learn

FoCS
	please track and go
	really worth your time

Lab Sessions
	Tue, 6-8pm, GDC 1.302
	Thu, 6-8pm, GDC 1.302

Office Hours
	Glenn
		MF, 12-12:45pm, GDC 6.302
		T,	 3-4pm,		Zoom, https://us04web.zoom.us/j/412178924

	Chenxi
		F, 2:30-3:30pm, GDC 1.302

	Hannah
		M, 2-3pm, GDC 1.302

	Abhinav
		W, 2-3pm, GDC 1.302

	Alvin
		T, 1-2pm, GDC 1.302

Canvas
	personal questions

Piazza
	class questions
	ask and answer questions
	please be proactive

Notes
	these notes are on GitLab and Piazza
"""

# Java

class A {
    ...}

A x = new A(...)
A y = new A(...)                # same content
System.out.println(x == y)      # false
System.out.println(x.equals(y)) # false

"""
__init__ is like a Java constructor
__eq__   is like Java's .equals
"""

# Java

int i = 2;
int j = 3;
int k = (i + j); # (2 + 3), C, C++, Java, Python

"""
the + operator can take two r-values
"""

i + j; # no

i += j; # C, C++, Java, Python
i += 3;
2 += j; # no

"""
the += operator takes an l-value and an r-value, and returns an r-value (NOT Python)
"""

int k = (i += j); # C, C++, Java, NOT Python

(i += j)++; # C++, NOT C, Java, Python

"""
a higher-order function is a function that either takes or returns another function
"""

v = reduce(add, ...)

"""
string's [] operator returns an r-value
list's   [] operator returns an l-value
tuple's  [] operator returns an r-value
"""
