// -----------
// Mon, 20 Apr
// -----------

/*
Cold Calling
	you're only called ONCE per rotation
	it's totally fine to be wrong, the idea is to discuss and to learn

FoCS
	please track and go
	really worth your time

Canvas
	personal questions

Piazza
	class questions
	ask and answer questions
	please be proactive

Notes
	these notes are on GitLab and Piazza
*/

/*
Refactoriing:
    Replace Temp with Query (120)
    repeated calls might be expensive
    intolerable with side effects

we don't worry about the cost until we're done refactoring
*/
