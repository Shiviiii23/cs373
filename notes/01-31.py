# -----------
# Fri, 31 Jan
# -----------

"""
Cold Calling
	you're only called ONCE per rotation
	it's totally fine to be wrong, the idea is to discuss and to learn

FoCS
	please track and go
	really worth your time

Lab Sessions
	Tue, 6-8pm, GDC 1.302
	Thu, 6-8pm, GDC 1.302

Office Hours
	Glenn
		MF, 12-12:45pm, GDC 6.302
		T,	 3-4pm,		Zoom, https://us04web.zoom.us/j/412178924

	Chenxi
		F, 2:30-3:30pm, GDC 1.302

	Hannah
		M, 2-3pm, GDC 1.302

	Abhinav
		W, 2-3pm, GDC 1.302

	Alvin
		T, 1-2pm, GDC 1.302

Canvas
	personal questions

Piazza
	class questions
	ask and answer questions
	please be proactive

Notes
	these notes are on GitLab and Piazza
"""

"""
mcl(10, 100)
b = 10
e = 100
m = 51
if b < m
if 10 < 51
mcl(10, 100) = mcl(51, 100)
"""

"""
unit tests are called white box tests
they require an understanding of the parts of the solution
"""

"""
acceptance tests are called black box tests
they only require understanding the input output behavior
"""

"""
configure code in a way that is easy to test

1. a kernel
    Collatz.py

2. a run harness
    RunCollatz.py

3. a test harness
    TestCollatz.py
"""

"""
read eval print loop (REPL)
"""
