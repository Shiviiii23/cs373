# -----------
# Fri, 10 Apr
# -----------

"""
Cold Calling
	you're only called ONCE per rotation
	it's totally fine to be wrong, the idea is to discuss and to learn

FoCS
	please track and go
	really worth your time

Canvas
	personal questions

Piazza
	class questions
	ask and answer questions
	please be proactive

Notes
	these notes are on GitLab and Piazza
"""

"""
natural join
    no matching attributes: cross join
    matching attributes, but no matching values: empty
    matching attributes and some matching values: a subset of cross join
"""

"""
student table (stud_id, name, gpa)
college table (name, state)
apply   table (st_id, major, decision)
"""

"""
C, Java, Python are procedural languages
SQL is a declarative language
"""

"""
line comment
    Python: #
    SQL:    --

block comment
    Python: three quotes
    SQL:    /* */
"""

"""
movie table

name year director genre
"star wars" 1977 "george lucas" "sci-fi"
"shane" 1953 "george stevens" "western"
...
"""

"""
director table

director_id name
1 "george lucas"
2 "george stevens"

movie table

name year dir_id genre
"star wars" 1977 1 "sci-fi"
"shane" 1953 2 "western"
...
"""

"""
innodb
    force the creation of director table first
    and will then enforce existing director ids
    force the tear down of movie table first
"""

"""
SQL select
    relational algebra select
    relational algebra project
"""
