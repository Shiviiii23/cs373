# -----------
# Mon, 10 Feb
# -----------

"""
Cold Calling
	you're only called ONCE per rotation
	it's totally fine to be wrong, the idea is to discuss and to learn

FoCS
	please track and go
	really worth your time

Lab Sessions
	Tue, 6-8pm, GDC 1.302
	Thu, 6-8pm, GDC 1.302

Office Hours
	Glenn
		MF, 12-12:45pm, GDC 6.302
		T,	 3-4pm,		Zoom, https://us04web.zoom.us/j/412178924

	Chenxi
		F, 2:30-3:30pm, GDC 1.302

	Hannah
		M, 2-3pm, GDC 1.302

	Abhinav
		W, 2-3pm, GDC 1.302

	Alvin
		T, 1-2pm, GDC 1.302

Canvas
	personal questions

Piazza
	class questions
	ask and answer questions
	please be proactive

Notes
	these notes are on GitLab and Piazza
"""

x == y
x.__eq__(y)

"""
list's += takes any iterable on the right hand side
list's +  only takes another list on the right hand side
"""

x += y
# is the same as (no, for lists; yes, for tuples)
x = x + y

"""
tuple's += only takes another tuple on the right hand side
tuple's +  only takes another tuple on the right hand side
"""

# Java

class A {
    static int j = 3;
    int i = 2;}

x = new A(...)
y = new A(...)

"""
CLOSED object model (same with C++)
all instances of A will share j and have their own i
over time the instances of A have the same data
"""

# Python

class A :
    j = 3
    def __init__ (self) : # doesn't have to be __init__
        self.i = 2

"""
OPEN object model
different instances can have different data
an instance over time can have different data
"""

x = A()
y = A()
x.m = 5
