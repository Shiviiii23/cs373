# -----------
# Mon, 27 Jan
# -----------

"""
Cold Calling
	you're only called ONCE per rotation
	it's totally fine to be wrong, the idea is to discuss and to learn

FoCS
	please track and go
	really worth your time

Lab Sessions
	Tue, 6-8pm, GDC 1.302
	Thu, 6-8pm, GDC 1.302

Office Hours
	Glenn
		MF, 12-12:45pm, GDC 6.302
		T,	 3-4pm,		Zoom, https://us04web.zoom.us/j/412178924

	Chenxi
		F, 2:30-3:30pm, GDC 1.302

	Hannah
		M, 2-3pm, GDC 1.302

	Abhinav
		W, 2-3pm, GDC 1.302

	Alvin
		T, 1-2pm, GDC 1.302

Canvas
	personal questions

Piazza
	class questions
	ask and answer questions
	please be proactive

Notes
	these notes are on GitLab and Piazza
"""

"""
no explanation needed for missing a paper, quiz, or blog
gitlab access is not required for public code repo
"""

"""
Python's unit test framework: the function, main, the class, TestCase
"""

"""
for Java inheritance: a class extends only one class, interface extends many interfaces
a class implements many interfaces
"""

"""
Python's self is equivalent to Java's this
"""

"""
Java
to call another method, "this." is optional
this is a keyword
"""

"""
Python
in Python to call another method, "self." is required
self is NOT a keyword
"""

"""
bad tests can hide bad code
"""

"""
1. run the code as is, confirm success
2. identify bad tests, fix them
3. run the code, confirm failure
4. identify bad code, fix that
5. run the code, confirm success
"""

def is_prime (n: int) -> bool :
    assert n > 0
    if (n == 2)
        return True
    if (n == 1) or ((n % 2) == 0) :
        return False
    for i in range(3, int(sqrt(n)) + 1, 2) :
        if (n % i) == 0 :
            return False
    return True
