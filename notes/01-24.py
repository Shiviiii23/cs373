# -----------
# Fri, 24 Jan
# -----------

"""
Cold Calling
	you're only called ONCE per rotation
	it's totally fine to be wrong, the idea is to discuss and to learn

FoCS
	please track and go
	really worth your time

Lab Sessions
	Tue, 6-8pm, GDC 1.302
	Thu, 6-8pm, GDC 1.302

Office Hours
	Glenn
		MF, 12-12:45pm, GDC 6.302
		T,	 3-4pm,		Zoom, https://us04web.zoom.us/j/412178924

	Chenxi
		F, 2:30-3:30pm, GDC 1.302

	Hannah
		M, 2-3pm, GDC 1.302

	Abhinav
		W, 2-3pm, GDC 1.302

	Alvin
		T, 1-2pm, GDC 1.302

Canvas
	personal questions

Piazza
	class questions
	ask and answer questions
	please be proactive

Notes
	these notes are on GitLab and Piazza
"""

"""
there's no need for an excuse or an explanation when you miss a blog, a paper, or a quiz
"""

"""
Persuall screenshot of the My Scores page on Canvas
"""

"""
# is the line comment, // in Java
three quotes is the block comment, /* */ in Java
"""

"""
Collatz Conjecture
about 100 years old

take a pos int
if even divide	 by 2
if odd	multiply by 3 and add 1
repeat until 1
"""

5 16 8 4 2 1

"""
the cycle length of	 5 is 6
the cycle length of 10 is 7
"""

"""
python is typeless
if type annotated, python will ignore the annotations, if they're legal
"""

"""
assertions are good for preconditions and postconditions

assertions are bad for testing,	   unit testing frameworks will be good for this
assertions are bad for user input, exceptions will be good for this
"""
