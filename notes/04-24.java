// -----------
// Fri, 24 Apr
// -----------

/*
Cold Calling
	you're only called ONCE per rotation
	it's totally fine to be wrong, the idea is to discuss and to learn

FoCS
	please track and go
	really worth your time

Canvas
	personal questions

Piazza
	class questions
	ask and answer questions
	please be proactive

Notes
	these notes are on GitLab and Piazza
*/

/*
Alvin: 1, 2, 3
Chenxi, 4, 5, 6
Glenn: 7, 8, 9, 10
*/

/*
Abhinash: 1, 2, 3
Hannah: 4, 5, 6
Glenn: 7, 8, 9, 10
*/

class A {}
class B extends A {}

class C {}
class D {
    private C x;}

/*
to eliminate the switch
    1. inheritance (NewMovie, ChildrensMovie, RegularMovie)
        a. we'd have to change the tests
        b. how will change the type, that would be hard

    2. containment (NewPrice, ChildrensPrice, RegularPrice)
        a. don't have to change the tests
        b. easy to change type
*/

class Mammal {}
class Tiger extends Mammal {}
class Lion  extends Mammal {}

class Test {
    public static void main (...) {
        Mammal x = new Tiger();
        x.foo();                // Tiger.foo

        Mammal y = new Lion;
        y.foo();                // Lion.foo
        }}
