# -----------
# Fri,  2 Mar
# -----------

"""
Cold Calling
	you're only called ONCE per rotation
	it's totally fine to be wrong, the idea is to discuss and to learn

FoCS
	please track and go
	really worth your time

Lab Sessions
	Tue, 6-8pm, GDC 1.302
	Thu, 6-8pm, GDC 1.302

Office Hours
	Glenn
		MF, 12-12:45pm, GDC 6.302
		T,	 3-4pm,		Zoom, https://us04web.zoom.us/j/412178924

	Chenxi
		F, 2:30-3:30pm, GDC 1.302

	Hannah
		M, 2-3pm, GDC 1.302

	Abhinav
		W, 2-3pm, GDC 1.302

	Alvin
		T, 1-2pm, GDC 1.302

Canvas
	personal questions

Piazza
	class questions
	ask and answer questions
	please be proactive

Notes
	these notes are on GitLab and Piazza
"""

# Java

class A {
    class B {...}
    static class C {...}}

class Test {
    public static void main (...) {
        A   x = new A(...)
        A.B y = new A.B(...) # no
        A.B z = x.new B(...)
        A.C t = new A.C(...)
        }}

# Python

class A :
    class B :

x = A(...)
y = A.B(...)

class square3 :
    def __call__ (self, v) :
        return v ** 2

x = square3(4)       # no
print(type(square3)) # type

f = square3()
print(type(f)) # square3

n = f(4)
print(n) # 16

y = list(...)
print(type(y)) # list

def test3 () :
    a = [2, 3, 4]
    m = map(square3,   a)        # no
    m = map(square3(), a)
    assert list(m) == [4, 9, 16]
    assert list(m) == []

def pow1 (p) :
    def f (v) :
        return v ** p
    return f

"""
f() needing to capture p is called a closure
"""

"""
three tokens
    =, * [two meanings], **

two contexts
    a function definition
    a function call

seven stories
"""
