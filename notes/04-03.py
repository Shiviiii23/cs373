# -----------
# Fri,  3 Apr
# -----------

"""
Cold Calling
	you're only called ONCE per rotation
	it's totally fine to be wrong, the idea is to discuss and to learn

FoCS
	please track and go
	really worth your time

Canvas
	personal questions

Piazza
	class questions
	ask and answer questions
	please be proactive

Notes
	these notes are on GitLab and Piazza
"""

"""
Park Protection

- expanded role
- evenness about commits, issues, unit tests
- tighten the model page in terms of white space
- plants need some expansion
- animals need some expansion

90 min in one

- expanded role
- evenness about commits, issues, unit tests
- some images distorted on About page
- tighten the model page in terms of white space
- consistent number of instances on model pages
- instance pages need multimedia and description

Pets 4 Me

- expanded role
- evenness about commits, issues, unit tests
- model pages, total number of instances
- quantitive data per instance on the model pages that we could filter or sort by
- instance pages, a lot more descriptions, some multimedia

Crash Safe

- expanded role
- consider some icons for the tools
- images are distorted
- model pages total number of pages
- turn some of those into table grids

Hide Advisor

- increase of the member photos
- expanded role
- evenness on About page for commits, issues, and tests
- icons for the tools on the About page
- model pages: total number of instances and the total number of pages
- instance pages: a lot of description, multimedia
- more interesting colors or images
- missing the connections to other instances

Keep Austin Safe

- About: maybe icons for the tools
- elaborate on the rolls
- more evenness on stats per member
- can't afford to only have table model pages
- instance to have more description
- making the links to the other instances more engaging

Metro Health

- expand the description of the roles
- evenness about issues, commits, tests
- make your images bigger
- icons for the tools
- don't want only tables for model pages
- instance pages images a bit bigger

The Funds Don't Stop

- elaborate on the rolls
- icons or images for the data links and tool links
- can't have zero tests!
- can't have all tables
- make some grids
- on the model pages have to have at least 5 attributes that are filterable, sortable
- instance pages, a lot more description, multimedia
- tighten up the presentation of the data

Climate Buddy

- should work the same on Chrome and Firefox
- work on different screen sizes
- splash page font change
- bios on About page are too small
- more evenness in issues and tests
- icons for tools in about page
- images for countries and cities pages
- table instead of cards for world view
- add 1 more attribute to the cards on worldview and countries
- total number of pages and instances
- numbers on pagination and jumping to last page
- Human Readable titles
- commas in statistics
- multimedia on instance pages
- blurbs on instance pages

Stock Me Out

- try a different color on the description text in splash page
- description on About page
- formatting on the About page cards
- make the role larger on about page
- bios on about page
- icons on tools in about page
- more evenness on commits
- more issues and tests
- work on different screen sizes
- more information on instance page
- blurb on instance page
- connections between the instance pages
- multimedia on instance pages

Food Ninja

- more issues
- add number of tests to About page
- icons in tools section of About page
- 5 attributes on every card on the model pages
- total number of pages and instances
- add units on the stats in vegetable instance page
- formatting on instance pages
- add connections from fruits and vegetables to recipes
- add description on nutrition instance pages
- add steps on recipe instance pages

The Buzzz:

- make splash search easier to see
- roles can be extended
- unittest could be more balanced
- add next page and prev page
- more visual interest in instance cards
- clicking on image does not go to instance
- in instances, show counstry names and not just ID
- more media in instance pages
- clean up data

Sweet Cities:

- need to assign unit tests
- icons for tools on abut page
- extend role descriptions
- pagination icons hard to see
- show total number instances
- show total number of pages
- more visual insterest for instances
- more interactive media
- more visual interest all around
- links needed to other instances

All Olympian:

- make splash page model links more visually interesting
- extend role descriptions
- fix commit number issue
- get those unit test numbers up
- look into image resizing
- maybe consider showing a subset of pages to click to
- more searchable/filterable attributes per tile on model page
- add more direct links from instances of one model to another
- more visual interest throughout

Danger Zone

- Add unit tests
- Add short desc. of member roles on about page
- Add short desc. of require tools used
- Add more instances and show total number of instances per page
- Add some info on each instance in the model page for each model
- Consider switching to a card view that shows more info on the model page
- Add text data for every instance
- Separate instance pages from the model pages
- Could use more media (Maps for each city)
- In health instances, add short desc. for each index to help explain what the number means
- Adding units for nuumerical data where appropriate
- Images displayed seems too random, a fitting image for each city would make more sense
- Overall could be more visually appealing

Freshman to Fresh Grad

- Fix multiple email bug on commit numbers, number of total issues & unit tests bugs
= More balanced unit testing and commit numbers
- Add short desc. of member roles on about page
= Add short desc. of each city, university, and company as text data
- Use a more accessible pagination bar on the bottom (with first/last page)
    - center the bar for a more appealing look
- Some instances are missing data/images that isn't available (hardcode it or remove it where appropriate)
- Company instance pages have too much white space
    - adding text data or resizing the image should help fill out the page
- Consistency in units (Graduation rate should be expressed as a percentage like all the other percentages)
- Formatting inconsistencies (ex. model page: employment should be followed by a colon)

Band Together

- Add more details describing the role of each of you
- Fix the 'page arrow' bug
- A place holder for search
- Multimedia for instances, e.g. video
- Balance the commits

College Fit For Me

- Add a short desc. of member roles on about page
- Show more instances per row by reducing the card size on model page
- Add text data for each instance of a city and a university
- Fix the stretching issue of college logos on instance page
- If possible, I think adding a map for each instance of an attraction would be helpful as well
- Show total number of instances per model
- Highlight the current page on the pagination bar
- Add First/Last page icon on pagination bar

Pathogerm

- add multimedia in the instance part, say images and videos
- a few more words about the role in the team
- balance the commits

Land It in Space

- add the number of total pages in ASTRONAUTS page
- add a paragraph of text in instance page
- more words about the role in the team
- balance the commits
"""
