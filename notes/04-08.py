# -----------
# Wed,  8 Apr
# -----------

"""
Cold Calling
	you're only called ONCE per rotation
	it's totally fine to be wrong, the idea is to discuss and to learn

FoCS
	please track and go
	really worth your time

Canvas
	personal questions

Piazza
	class questions
	ask and answer questions
	please be proactive

Notes
	these notes are on GitLab and Piazza
"""

"""
student table
college table
apply   table
"""

"""
theta join
takes three arguments
    a relation
    another relation
    a binary predicate
"""

def cross_join (r, s) :
    ...

def theta_join (r, s, bp) :
    t = cross_join(r, s)
    return select(t, bp)    # not quite right, but too expensive, anyway

"""
natural join
takes two arguments
    a relation
    another relation
"""

def theta_join (r, s, bp) :
    ...

def natural_join (r, s) :
    def bp (u, v) :
        ...
    return theta_join(r, s, bp)
