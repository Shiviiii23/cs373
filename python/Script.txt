% which docker
/usr/local/bin/docker



% docker --version
Docker version 18.09.1, build 4c52b90



% docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE



% cat Dockerfile
FROM python:3

RUN apt-get update

RUN apt-get -y install libgmp-dev
RUN apt-get -y install vim

RUN pip install --upgrade pip
RUN pip --version

RUN pip install black
RUN pip install coverage
RUN pip install mypy
RUN pip install numpy
RUN pip install pylint

RUN git clone https://github.com/DOMjudge/checktestdata checktestdata && \
    cd checktestdata                                                  && \
    git checkout release                                              && \
    ./bootstrap                                                       && \
    make                                                              && \
    cp checktestdata /usr/bin                                         && \
    cd -

CMD bash



% docker build -t gpdowning/python .
...



% docker login
...



% docker push gpdowning/python
...



% docker pull gpdowning/python
...



% docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
gpdowning/python    latest              5b1594ea10c1        2 hours ago         1.07GB
python              3                   825141134528        3 weeks ago         923MB



% pwd
/Users/downing/cs373/git


% ls
README	_CS373.bbprojectd  examples  makefile  notes  projects



% docker run -it -v /Users/downing/cs373/git:/usr/python -w /usr/python gpdowning/python

root@a4fe84a658f0:/usr/python# pwd
/usr/python



root@a4fe84a658f0:/usr/python# ls
README	_CS373.bbprojectd  examples  makefile  notes  projects



root@a4fe84a658f0:/usr/python# which black
/usr/local/bin/black
root@a4fe84a658f0:/usr/python# black --version
black, version 18.9b0



root@a4fe84a658f0:/usr/gcc# which checktestdata
/usr/local/bin/checktestdata
root@c70871d08248:/usr/gcc# checktestdata --version
checktestdata -- version 20190202, written by Jan Kuipers, Jaap Eldering, Tobias Werth



root@a4fe84a658f0:/usr/python# which coverage
/usr/local/bin/coverage
root@a4fe84a658f0:/usr/python# coverage --version
Coverage.py, version 4.5.2 with C extension



root@a4fe84a658f0:/usr/python# which git
/usr/bin/git
root@a4fe84a658f0:/usr/python# git --version
git version 2.11.0



root@a4fe84a658f0:/usr/python# which  make
/usr/bin/make
root@a4fe84a658f0:/usr/python# make --version
GNU Make 4.1



root@a4fe84a658f0:/usr/python# which mypy
/usr/local/bin/mypy
root@a4fe84a658f0:/usr/python# mypy --version
mypy 0.660



root@a4fe84a658f0:/usr/python# which pip
/usr/local/bin/pip
root@a4fe84a658f0:/usr/python# pip --version
pip 18.1 from /usr/local/lib/python3.7/site-packages/pip (python 3.7)



root@a4fe84a658f0:/usr/python# which pydoc
/usr/local/bin/pydoc
root@a4fe84a658f0:/usr/python# pydoc --version
...



root@a4fe84a658f0:/usr/python# which pylint
/usr/local/bin/pylint
root@a4fe84a658f0:/usr/python# pylint --version
pylint 2.2.2



root@a4fe84a658f0:/usr/python# which python
/usr/local/bin/python
root@a4fe84a658f0:/usr/python# python --version
Python 3.7.2



root@a4fe84a658f0:/usr/python# which vim
/usr/bin/vim
root@a4fe84a658f0:/usr/python# vim --version
VIM - Vi IMproved 8.0 (2016 Sep 12, compiled Sep 30 2017 18:21:38)
