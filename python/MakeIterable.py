#!/usr/bin/env python3

# pylint: disable = bad-whitespace
# pylint: disable = eval-used
# pylint: disable = invalid-name
# pylint: disable = missing-docstring
# pylint: disable = too-few-public-methods

# ---------------
# MakeIterable.py
# ---------------

@make_iterable
def my_range (b, e) :
    while b != e :
        yield b
        b += 1

map_iterable = make_iterable(map)

def test0 () :
    x = my_range(2, 5)
    assert not hasattr(x, "__next__")
    assert     hasattr(x, "__iter__")
    p = iter(x)
    assert     hasattr(p, "__next__")
    assert     hasattr(p, "__iter__")
    assert iter(p) is p
    assert list(x) == [2, 3, 4]
    assert list(x) == [2, 3, 4]

def test1 () :
    x = map_iterable(lambda v : v ** 2, (2, 3, 4))
    assert not hasattr(x, "__next__")
    assert     hasattr(x, "__iter__")
    p = iter(x)
    assert     hasattr(p, "__next__")
    assert     hasattr(p, "__iter__")
    assert list(x) == [4, 9, 16]
    assert list(x) == [4, 9, 16]

def main () :
    print("MakeIterable")
    for n in range(2) :
        eval("test" + str(n) + "()")
    print("Done.")

if __name__ == "__main__" : # pragma: no cover
    main()
