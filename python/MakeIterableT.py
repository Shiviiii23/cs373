#!/usr/bin/env python3

# pylint: disable = bad-whitespace
# pylint: disable = eval-used
# pylint: disable = invalid-name
# pylint: disable = missing-docstring
# pylint: disable = too-few-public-methods

# ---------------
# MakeIterable.py
# ---------------

from unittest import main, TestCase

def make_iterable (iterator) :
    class iterable :
        def __init__ (self, *t, **d) :
            self.t = t
            self.d = d

        def __iter__ (self) :
            return iterator(*self.t, **self.d)

    return iterable

@make_iterable
def my_range (b, e) :
    while b != e :
        yield b
        b += 1

map_iterable = make_iterable(map)

class MyUnitTests (TestCase) :
    def test0 (self) :
        x = my_range(2, 5)
        self.assertFalse(hasattr(x, "__next__"))
        self.assertTrue(hasattr(x, "__iter__"))
        p = iter(x)
        self.assertTrue(hasattr(p, "__next__"))
        self.assertTrue(hasattr(p, "__iter__"))
        self.assertEqual(list(x), [2, 3, 4])
        self.assertEqual(list(x), [2, 3, 4])

    def test1 (self) :
        x = map_iterable(lambda v : v ** 2, (2, 3, 4))
        self.assertFalse(hasattr(x, "__next__"))
        self.assertTrue(hasattr(x, "__iter__"))
        p = iter(x)
        self.assertTrue(hasattr(p, "__next__"))
        self.assertTrue(hasattr(p, "__iter__"))
        self.assertEqual(list(x), [4, 9, 16])
        self.assertEqual(list(x), [4, 9, 16])

if __name__ == "__main__" :
    main()
