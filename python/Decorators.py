#!/usr/bin/env python3

# pylint: disable = bad-whitespace
# pylint: disable = eval-used
# pylint: disable = invalid-name
# pylint: disable = missing-docstring
# pylint: disable = too-few-public-methods
# pylint: disable = unidiomatic-typecheck

# -------------
# Decorators.py
# -------------

from types import FunctionType

def f1 (n) :
    return n + 1

def test1 () :
    assert f1(2) == 3





def debug_function (f) :
    def g (n) :
        print(f.__name__, ":", end=" ")
        print("input =", n, ";", end=" ")
        m = f(n)
        print("output =", m, ";")
        return m
    return g

f2 = debug_function(f1)
assert type(f2) is FunctionType

def test2 () :
    assert f2(2) == 3 # f2 : input = 2 ; output = 3 ;

@debug_function
def f3 (n) :
    return n + 1
assert type(f3) is FunctionType

def test3 () :
    assert f3(2) == 3 # f3 : input = 2 ; output = 3 ;





class debug_class :
    def __init__ (self, f) :
        self.f = f

    def __call__ (self, n) :
        print(self.f.__name__, ":", end=" ")
        print("input =", n, ";", end=" ")
        m = self.f(n)
        print("output =", m, ";")
        return m

f4 = debug_class(f1)
assert type(f4) is debug_class

def test4 () :
    assert f4(2) == 3 # f4 : input = 2 ; output = 3 ;

@debug_class
def f5 (n) :
    return n + 1
assert type(f5) is debug_class

def test5 () :
    assert f5(2) == 3 # f5 : input = 2 ; output = 3 ;





def main () :
    print("Decorators.py")
    for n in range(5) :
        eval("test" + str(n + 1) + "()")
    print("Done.")

if __name__ == "__main__" : # pragma: no cover
    main()
