#!/usr/bin/env python3

# pylint: disable = bad-whitespace
# pylint: disable = eval-used
# pylint: disable = invalid-name
# pylint: disable = missing-docstring
# pylint: disable = redefined-outer-name

# ----------
# IsPrime.py
# ----------

from math import sqrt

def is_prime (n: int) -> bool :
    assert n > 0
    if (n == 1) or ((n % 2) == 0) :
        return False
    for i in range(3, int(sqrt(n))) :
        if (n % i) == 0 :
            return False
    return True

def test0 () :
    assert not is_prime(1)

def test1 () :
    assert not is_prime(2)

def test2 () :
    assert is_prime(3)

def test3 () :
    assert not is_prime(4)

def test4 () :
    assert is_prime(5)

def test5 () :
    assert is_prime(7)

def test6 () :
    assert is_prime(9)

def test7 () :
    assert not is_prime(27)

def test8 () :
    assert is_prime(29)

if __name__ == "__main__" : # pragma: no cover
    print("IsPrime.py")
    for n in range(8) :
        eval("test" + str(n) + "()")
    print("Done.")
