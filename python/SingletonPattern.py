#!/usr/bin/env python3

# pylint: disable = bad-whitespace
# pylint: disable = eval-used
# pylint: disable = global-variable-undefined
# pylint: disable = invalid-name
# pylint: disable = missing-docstring
# pylint: disable = no-self-use
# pylint: disable = not-callable
# pylint: disable = too-few-public-methods

# -------------------
# SingletonPattern.py
# -------------------

from types  import FunctionType
from typing import Dict

class A :
    def f (self) :
        return "A.f()"

def test1 () :
    global A

    assert isinstance(A, type)
    x = A()
    assert isinstance(x, A)
    assert x.f() ==     "A.f()"
    y = A()
    assert isinstance(y, A)
    assert y     is not x
    assert y.f() ==     "A.f()"
    z = type(x)()
    assert isinstance(z, A)
    assert z     is not x
    assert z.f() ==     "A.f()"

    A = A()
    assert A.f() ==     "A.f()"
    y = type(A)()
    assert y     is not A
    assert y.f() ==     "A.f()"



def SingletonDecorator1 (c) :
    x = c()
    return lambda : x

@SingletonDecorator1
class B :
    def f (self) :
        return "B.f()"

def test2 () :
    assert isinstance(B, FunctionType)
    x = B()
    assert x.f() ==       "B.f()"
    y = B()
    assert y       is     x
    z = type(x)()
    assert z       is not x
    assert z.f()   ==     "B.f()"



def SingletonDecorator2 (c) :
    x = None
    def g () :
        nonlocal x
        if x is None :
            x = c()
        return x
    return g

@SingletonDecorator2
class C :
    def f (self) :
        return "C.f()"

def test3 () :
    assert isinstance(C, FunctionType)
    x = C()
    assert x.f() ==       "C.f()"
    y = C()
    assert y       is     x
    z = type(x)()
    assert z       is not x
    assert z.f()   ==     "C.f()"



class D :
    __d: Dict = {}

    def __init__ (self):
        self.__dict__ = self.__d

    def f (self) :
        return "D.f()"

def test4 () :
    assert isinstance(D, type)
    x = D()
    assert isinstance(x, D)
    assert x.f()      ==     "D.f()"
    y = D()
    assert isinstance(y, D)
    assert y          is not x
    assert y.f()      ==     "D.f()"
    assert x.__dict__ is     y.__dict__

def main () :
    print("SingletonPattern.py")
    for i in range(4) :
        eval("test" + str(i + 1) + "()")
    print("Done.")

if __name__ == "__main__" : # pragma: no cover
    main()
