#!/usr/bin/env python3

# pylint: disable = bad-whitespace
# pylint: disable = eval-used
# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# ------------
# Factorial.py
# ------------

# https://docs.python.org/3.6/library/math.html

from math import factorial

def test0 () :
    assert factorial(0) == 1

def test1 () :
    assert factorial(1) == 1

def test2 () :
    assert factorial(2) == 2

def test3 () :
    assert factorial(3) == 6

def test4 () :
    assert factorial(4) == 24

def test5 () :
    assert factorial(5) == 120

def main () :
    print("Factorial.py")
    for n in range(6) :
        eval("test" + str(n) + "()")
    print("Done.")

if __name__ == "__main__" : # pragma: no cover
    main()
