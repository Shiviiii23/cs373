#!/usr/bin/env python3

# pylint: disable = bad-whitespace
# pylint: disable = eval-used
# pylint: disable = invalid-name
# pylint: disable = missing-docstring
# pylint: disable = no-self-use
# pylint: disable = too-few-public-methods
# pylint: disable = unused-variable

# -------------
# Reflection.py
# -------------

print("Reflection.py")

class A () :
    def f (self) :
        return "A.f()"

def test1 () :
    x = A()
    assert isinstance(x, A)
    assert x.f() == "A.f()"

def test2 () :
    x = A()
    c = type(x)
    assert isinstance(c, type)
    y = c()
    assert isinstance(y, A)
    assert y is not x
    assert y.f() == "A.f()"

def test3 () :
    x = A()
    c1 = type(x)
    c2 = x.__class__
    assert isinstance(c2, type)
    assert c2 is c1
    y = c2()
    assert isinstance(y, A)
    assert y is not x
    assert y.f() == "A.f()"

def test4 () :
    x = A()
    c1 = type(x)
    d = globals()
    assert isinstance(d, dict)
    c2 = d["A"]
    assert isinstance(c2, type)
    assert c2 is c1
    y = c2()
    assert isinstance(y, A)
    assert y is not x
    assert y.f() == "A.f()"

class B () :
    def __init__ (self, v) :
        self.v = v

def test5 () :
    try :
        c = globals()["B"]
        x = c()
        assert False
    except TypeError as e :
        assert e.args == ("__init__() missing 1 required positional argument: 'v'",)

def test6 () :
    try :
        c = globals()["C"]
        assert False
    except KeyError as e :
        assert e.args == ('C',)

def main () :
    for n in range(6) :
        eval("test" + str(n + 1) + "()")

if __name__ == "__main__" : # pragma: no cover
    print("Reflection.py")
    main()
    print("Done.")
