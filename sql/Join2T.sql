-- ----------
-- Join2T.sql
-- ----------

use test;

-- -----------------------------------------------------------------------
-- ID, name, and GPA of students who applied in CS
-- MUST USE inner join

select "*** query #1 ***";
select distinct sID, sName, GPA
    from Student
    inner join Apply using (sID)
    where major = "CS";

-- -----------------------------------------------------------------------
-- ID, name, and GPA of students who applied in CS
-- MUST USE subquery

select "*** query #2a ***";
select sID, sName, GPA
    from Student
    where sID in
        (select sID
            from Apply
            where major = "CS");

-- with distinct
select "*** query #2b ***";
select sID, sName, GPA
    from Student
    where sID in
        (select distinct sID
            from Apply
            where major = "CS");

-- -----------------------------------------------------------------------
-- GPA of students who applied in CS
-- sorted in descending order

-- this is not right, why?
select "*** query #3a ***";
select GPA
    from Student
    inner join Apply using (sID)
    where major = "CS"
    order by GPA desc;

-- this is still not right, why?
select "*** query #3b ***";
select distinct GPA
    from Student
    inner join Apply using (sID)
    where major = "CS"
    order by GPA desc;

-- this is right
select "*** query #3c ***";
select GPA
    from Student
    where sID in
        (select sID
            from Apply
            where major = "CS")
    order by GPA desc;

-- this is also right (with distinct)
select "*** query #3d ***";
select GPA
    from Student
    where sID in
        (select distinct sID
            from Apply
            where major = "CS")
    order by GPA desc;

-- -----------------------------------------------------------------------
-- ID of students who have applied in CS but not in EE

-- this is not right, why?
select "*** query #4a ***";
select sID
    from Student
    where
        sID in (select sID from Apply where major  = "CS")
        and
        sID in (select sID from Apply where major != "EE");

-- this is right
select "*** query #4b ***";
select sID
    from Student
    where
        sID     in (select sID from Apply where major = "CS")
        and
        sID not in (select sID from Apply where major = "EE");

-- this is also right
select "*** query #4c ***";
select distinct sID
    from Apply
    where
        (major = "CS")
        and
        sID not in (select sID from Apply where major = "EE");

-- -----------------------------------------------------------------------
-- name and enrollment of college with highest enrollment

-- subquery (with not exists)
select "*** query #5a ***";
select cName, enrollment
    from College as R
    where not exists
        (select *
            from College as S
            where R.enrollment < S.enrollment);

-- subquery (with all)
select "*** query #5b ***";
select cName, enrollment
    from College
    where enrollment >= all
        (select enrollment
            from College);

-- -----------------------------------------------------------------------
-- ID, name, and GPA of student with highest GPA

-- this is not right, why?
select "*** query #6a ***";
select sID, sName, GPA
    from Student as R
    where not exists
        (select *
            from Student as S
            where R.GPA < S.GPA);

-- this is right
select "*** query #6b ***";
select sID, sName, GPA
    from Student as R
    where
        not exists
            (select *
                from Student as S
                where R.GPA < S.GPA)
        and
        (GPA is not null);

-- this is also right, subquery (with all)
select "*** query #6c ***";
select sID, sName, GPA
    from Student
    where GPA >= all
        (select GPA
            from Student
            where GPA is not null);

exit
